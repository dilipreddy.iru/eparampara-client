import styled from 'styled-components/native';
import { ListItem, Icon, Body, Right } from 'native-base';


export const IconWrapperView = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: space-around;
`;
export const IconWrapperViewSub = styled.View`
  flex-direction: row;
`;

export const RightView = styled(Right)`
  flex: 1;
`;