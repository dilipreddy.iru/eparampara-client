import React, { Component } from 'react';
// import {View } from 'react-native'
import axios from "axios";
import { TouchableOpacity } from "react-native";

import { Container, Header, Content, List, ListItem, Text, Left, Right, Button, Body, Icon } from 'native-base';
import {
  IconWrapperView,
  IconWrapperViewSub, RightView
} from './styled.js'
export default class EmployeeList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      employeesData: [],
      token: '',
      selectCountry: '',
    };

    this.onDelete = this.onDelete.bind(this)
  }
  async componentDidMount() {
    axios.get("http://192.168.43.230:4000/employees").then((response) => {
      console.log("response", response.data);
      this.setState({
        employeesData: response.data
      })
    })
  }

  onDelete = (id) => {
    console.log('if', id);

    axios.delete(`http://192.168.43.230:4000/employees/${id}`, { params: { id } }).then(response => {
      console.log(response);
    });

  }

 


  render() {
    return (
      <Container>
        <Content>
          {/* <Header style={{backgroundColor: 'none'}} /> */}
          <Header style={{ alignItems: 'center', color: 'white' }}>
          <Text style={{color: 'white', fontSize: 20}}>Employee List</Text>
        </Header>
          {this.state.employeesData && this.state.employeesData.map((item) => {
            return (

              <List>
                <ListItem style={{ backgroundColor: '#5CFFF4'}} noIndent>
                  <Body>
                    {
                      item.name !== '' && (
                        <Text>{item.name}</Text>
                      )
                    }
                    {
                      item.address !== '' && (
                        <Text>{item.address}</Text>
                      )
                    }
                  </Body>
                  <RightView>
                    <IconWrapperView>
                      <IconWrapperViewSub>
                        <Button rounded onPress={() => {
                          this.props.navigation.push('Home',
                            {
                              name: item.name,
                              code: item.code,
                              address: item.address,
                              isEdit: true,
                              id: item._id
                            });
                        }} style={{ margin: 10, width: 50, textAlign: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor:'#42A4EB' }}>
                          <Icon style={{ fontSize: 18 }} name="edit-2" type="Feather" />
                        </Button>
                      </IconWrapperViewSub>
                      <IconWrapperViewSub>
                        <Button rounded onPress={() => this.onDelete(item._id)} style={{ margin: 10, width: 50, textAlign: 'center', alignItems: 'center', justifyContent: 'center', backgroundColor: '#FF6A3D' }}>
                          {/* <Text>Register</Text> */}
                          <Icon style={{ fontSize: 18 }} name="delete" type="MaterialCommunityIcons" />
                        </Button>
                      </IconWrapperViewSub>
                    </IconWrapperView>
                  </RightView>
                </ListItem>
              </List>

            )
          })}
        </Content>
      </Container>
    );
  }
}