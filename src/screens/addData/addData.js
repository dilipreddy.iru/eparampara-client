import React, { Component } from 'react';
import { TouchableOpacity } from "react-native";
import axios from "axios";
import { Container, Header, Content, Item, Input, Button, Text, Right, Toast } from 'native-base';



export default class HomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            count: 0,
            name: '',
            code: '',
            address: ''
        };
        this.onRegister = this.onRegister.bind(this)
        this.onEdit = this.onEdit.bind(this)
    }
    onNameChange(value) {
        console.log(value);

        this.setState({
            name: value
        });
    }
    onCodeChange(value) {
        this.setState({
            code: value
        });
    }
    onAddressChange(value) {
        this.setState({
            address: value
        });
    }
    componentDidMount() {
        let editName = this.props.navigation.getParam('name');
        let editCode = this.props.navigation.getParam('code');
        let editAddress = this.props.navigation.getParam('address');
        if(editName && editCode && editAddress) {
            this.setState({
            name: editName,
            code: editCode,
            address: editAddress
        })
        }
        
    }
    onRegister = () => {
        if (this.state.name === '' || this.state.code === '' || this.state.address === '') {
            alert('please enter all fields')

        } else {
            axios.post(
                'http://192.168.43.230:4000/employees',
                {
                    name: this.state.name,
                    code: this.state.code,
                    address: this.state.address
                    //other data key value pairs
                }).then((response) => {
                    if (response.data.isValid === true) {
                        alert('Employee Added Successfully')
                        this.setState({
                            name: '',
                            code: '',
                            address: ''
                        })
                    }
                    console.log('post res', response);

                })
            // this.setState({
            //   count: this.state.count + 1
            // });
        };
    }

    onEdit = (id) => {
        console.log('if', id);
    
        axios.patch(`http://192.168.43.230:4000/employees/${id}`).then(response => {
            this.setState({name : response.data.name, address : response.data.address})
            this.props.navigation.push('EmployeeList')
        });
    
      }

    render() {
        let isEdit = this.props.navigation.getParam('isEdit');
        let id = this.props.navigation.getParam('id');
        console.log('id render', id);
        
        return (
            <Container>
                <Header>
                    <Right>
                        <Button rounded onPress={() => this.props.navigation.navigate('Login')} style={{ margin: 10, textAlign: 'center', alignItems: 'center', justifyContent: 'center' }}>
                            <Text>Login</Text>
                        </Button>
                    </Right>
                </Header>
                <Content style={{ backgroundColor: '#5CFFF4'}}>
                    <Item rounded style={{ margin: 10 }}>
                        <Input placeholder='Employee Name' style={{borderColor: '#3D71FF', borderWidth: 1, borderRadius: 10}} value={this.state.name} onChangeText={val => {
                            this.onNameChange(val);
                        }} />
                    </Item>
                    <Item rounded style={{ margin: 10 }}>
                        <Input placeholder='Employee Code' style={{borderColor: '#3D71FF', borderWidth: 1, borderRadius: 10}} value={this.state.code} onChangeText={val => {
                            this.onCodeChange(val);
                        }} />
                    </Item>
                    <Item rounded style={{ margin: 10 }}>
                        <Input placeholder='Employee Address' style={{borderColor: '#3D71FF', borderWidth: 1, borderRadius: 10}} value={this.state.address} onChangeText={val => {
                            this.onAddressChange(val);
                        }} />
                    </Item>
                    <TouchableOpacity>
                        {
                            isEdit === true ?
                                <Button rounded onPress={() => this.onEdit(id)} style={{ margin: 10, textAlign: 'center', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text>Edit</Text>
                                </Button> : <Button rounded onPress={this.onRegister} style={{ margin: 10, textAlign: 'center', alignItems: 'center', justifyContent: 'center' }}>
                                    <Text>Register</Text>
                                </Button>
                        }
                    </TouchableOpacity>
                </Content>
            </Container>
        );
    }
}