import React, { Component } from 'react';
import { TouchableOpacity, StyleSheet } from "react-native";
import axios from "axios";
import { Container, Header, Content, Item, Input, Button, Text, Right } from 'native-base';

export default class Login extends Component {

  constructor(props) {
    super(props);
    this.state = { email: '', password: '' };
  }
  onEmailChange(value) {
    this.setState({
      email: value
    });
  }
  onPasswordChange(value) {
    this.setState({
      password: value
    });
  }

  onPress = () => {
    const newItem = {
      email: this.state.email,
      password: this.state.password
    }
    let self = this;
    axios.post('http://192.168.43.230:4000/user/login', newItem)
      .then(function (response) {
        console.log(response.data);
        if (!response.data.token) {
          console.log("please login");
        } else {
          self.props.navigation.push('EmployeeList');
        }
      })
      .catch(function (error) {
        console.log(error);
      });

  };

  render() {
    return (
      <Container style={styles.container}>
        <Header style={{ alignItems: 'center' }}>
          <Text style={{color: 'white', fontSize: 20}}>Login</Text>
        </Header>
        <Content style={{ backgroundColor: '#5CFFF4'}}>
          <Item rounded style={{ margin: 10 }}>
            <Input placeholder='Username' style={{borderColor: '#3D71FF', borderWidth: 1, borderRadius: 10}} value={this.state.email} onChangeText={val => {
              this.onEmailChange(val);
            }} />
          </Item>
          <Item rounded style={{ margin: 10 }}>
            <Input placeholder='PassWord' style={{borderColor: '#3D71FF', borderWidth: 1, borderRadius: 10}} value={this.state.password} onChangeText={val => {
              this.onPasswordChange(val);
            }} />
          </Item>
          <TouchableOpacity>
            <Button rounded onPress={this.onPress} style={{ margin: 10, textAlign: 'center', alignItems: 'center', justifyContent: 'center' }}>
              <Text>Login</Text>
            </Button>
          </TouchableOpacity>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  content: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center'
  },
  form: {
    width: '100%'
  },
  item: {}
});