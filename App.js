import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from "react-navigation-stack";

import HomeScreen from './src/screens/addData/addData';
import Login from './src/screens/Login/Login';
import EmployeeList from './src/screens/EmployeeList/EmployeeList'


export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}

const AppNavigator = createStackNavigator({
  
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      header: null,
  },
  },
  Login: {
    screen: Login,
    navigationOptions: {
      header: null,
  },
  },
  EmployeeList: {
    screen: EmployeeList,
    navigationOptions: {
      header: null,
  },
  }
});

const AppContainer = createAppContainer(AppNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});